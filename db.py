import os
from decimal import Decimal

from sqlalchemy import (
    Column, Date, Integer, String, UniqueConstraint, create_engine, func)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import label


DEFAULT_DB_PATH = os.path.join(
    os.path.dirname(os.path.realpath(__file__)), 'db.sqlite')

Base = declarative_base()


class HistoricalPrices(Base):
    __tablename__ = 'historical_prices'

    id = Column(Integer, primary_key=True)
    commodity = Column(String(10), nullable=False)
    # sqlite doesn't have native DECIMAL type, use String for prices
    price = Column(String(10), nullable=False)
    date = Column(Date, nullable=False)

    __table_args__ = (
        UniqueConstraint('commodity', 'date', name='_commodity_date_uc'),)

    def __repr__(self):
        return '{}({}, {})'.format(self.commodity, self.date, self.price)


class Db:
    def __init__(self, db_file_path=DEFAULT_DB_PATH):
        connection_string = self.get_connection_str(db_file_path)
        self.engine = create_engine(connection_string)
        Session = sessionmaker(bind=self.engine)
        self.session = Session()
        Base.metadata.create_all(bind=self.engine)

    @staticmethod
    def get_connection_str(path):
        return 'sqlite:///' + os.path.abspath(path)

    def get_last_row_for_commodity(self, commodity):
        return self.session.query(
            HistoricalPrices,
            label('max_date', func.max(HistoricalPrices.date))).filter_by(
                commodity=commodity).first()[0]

    def load_data(self, commodity, rows):
        last_loaded_row = self.get_last_row_for_commodity(commodity)
        for date, price in rows:
            if last_loaded_row and date < last_loaded_row.date:
                continue
            elif last_loaded_row and date == last_loaded_row.date:
                updated_price = last_loaded_row
                updated_price.price = price
            else:
                price_data = HistoricalPrices(
                    commodity=commodity, date=date, price=price)
                self.session.add(price_data)
        self.session.commit()

    def get_prices_between_dates(self, commodity, start_date, end_date):
        commodity_data = self.session.query(HistoricalPrices).filter(
            HistoricalPrices.commodity == commodity,
            HistoricalPrices.date >= start_date,
            HistoricalPrices.date <= end_date).all()
        return [Decimal(row.price) for row in commodity_data]
