#!/usr/bin/env python3

import contextlib
import datetime
import locale

import requests
from lxml import html

from db import Db


USER_AGENT = 'Mozilla/5.0'
COMMODITY_URLS = {
    'gold': 'https://www.investing.com/commodities/gold-historical-data',
    'silver': 'https://www.investing.com/commodities/silver-historical-data',
}

TBODY_SELECTOR = '//table[@id="curr_table"]/tbody'
DATE_COLUMN = 1
PRICE_COLUMN = 2


def get_table_rows(url, tbody_selector, columns):
    try:
        response = requests.get(url, headers={'User-Agent': USER_AGENT})
    except requests.ConnectionError as e:
        print(e)
        return []
    if response.status_code != 200:
        print('Request to {} returned status {}'.format(
            url, response.status_code))
        return []
    tree = html.fromstring(response.content)
    tables = tree.xpath(tbody_selector)
    if len(tables) != 1:
        print('Expected to find 1 table by selector {}, found {}'.format(
            tbody_selector, len(tables)))
        return []
    table = tables[0]
    return zip(*[
        table.xpath('tr/td[{}]/text()'.format(column_num))
        for column_num in columns])


@contextlib.contextmanager
def set_locale(*args, **kw):
    saved = locale.setlocale(locale.LC_ALL)
    yield locale.setlocale(*args, **kw)
    locale.setlocale(locale.LC_ALL, saved)


def convert_date_str(date_str):
    with set_locale(locale.LC_ALL, 'en_US.utf8'):
        return datetime.datetime.strptime(date_str, '%b %d, %Y').date()


def convert_price_str(price_str):
    return price_str.replace(',', '').strip()


def prepare_rows_for_loading(rows):
    return [
        (convert_date_str(row[0]), convert_price_str(row[1]))
        for row in rows if all(row)]


def scrape_historical_prices(url):
    rows = get_table_rows(url, TBODY_SELECTOR, [DATE_COLUMN, PRICE_COLUMN])
    return prepare_rows_for_loading(rows)


if __name__ == '__main__':
    db = Db()
    for commodity_name, url in COMMODITY_URLS.items():
        db.load_data(commodity_name, scrape_historical_prices(url))
