import datetime
from decimal import Decimal

from db import HistoricalPrices


def test_load_data_new_record(db, gold_price):
    loaded_date = gold_price.date + datetime.timedelta(days=1)
    loaded_price = '10.0'
    db.load_data('gold', [(loaded_date, loaded_price)])
    assert db.session.query(HistoricalPrices).filter_by(
        commodity='gold').count() == 2
    assert db.session.query(HistoricalPrices).filter_by(
        commodity='gold', date=loaded_date).one().price == loaded_price


def test_load_data_updates_last_date(db, gold_price):
    loaded_date = gold_price.date
    loaded_price = '111.0'
    db.load_data('gold', [(loaded_date, loaded_price)])
    assert db.session.query(HistoricalPrices).filter_by(
        commodity='gold').count() == 1
    assert db.session.query(HistoricalPrices).filter_by(
        commodity='gold').one().price == loaded_price


def test_prices_between_dates(db, gold_price):
    prices = db.get_prices_between_dates(
        'gold', gold_price.date, gold_price.date + datetime.timedelta(days=1))
    assert len(prices) == 1
    assert prices[0] == Decimal(gold_price.price)
    prices = db.get_prices_between_dates(
        'gold', gold_price.date - datetime.timedelta(days=1), gold_price.date)
    assert len(prices) == 1
    assert prices[0] == Decimal(gold_price.price)
    prices = db.get_prices_between_dates(
        'gold', gold_price.date - datetime.timedelta(days=2),
        gold_price.date - datetime.timedelta(days=1))
    assert not prices
