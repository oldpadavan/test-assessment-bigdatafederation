import datetime
import os
from unittest.mock import patch

from scraper import scrape_historical_prices


@patch('requests.get')
def test_scrape_historical_prices(mock_get):
    mock_get.return_value.status_code = 200
    with open(os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            'gold-historical-data')) as f:
        mock_get.return_value.content = f.read()

    scraped_rows = scrape_historical_prices('dummy.url')
    assert len(scraped_rows) == 2
    assert scraped_rows[0][0] == datetime.date(2018, 11, 23)
    assert scraped_rows[0][1] == '1223.20'
    assert scraped_rows[1][0] == datetime.date(2018, 11, 22)
    assert scraped_rows[1][1] == '1228.00'
