import datetime

import pytest

from getCommodityPrice import get_parser, validate_args


def test_parser_valid_args():
    p = get_parser()
    args = p.parse_args(['2018-07-06', '2018-08-09', 'gold'])
    assert args.start_date == datetime.date(2018, 7, 6)
    assert args.end_date == datetime.date(2018, 8, 9)
    assert args.commodity == 'gold'


@pytest.mark.parametrize('args_input,expected_error', [
    (['07-06-2018', '2018-08-09', 'gold'], 'start_date: invalid date format'),
    (['2018-07-06', '08-09-2018', 'gold'], 'end_date: invalid date format')])
def test_parser_invalid_args(args_input, expected_error, capsys):
    p = get_parser()
    with pytest.raises(SystemExit):
        p.parse_args(args_input)
    _, err = capsys.readouterr()
    assert expected_error in err


def test_validate_args(capsys):
    p = get_parser()
    args = p.parse_args(['2018-07-06', '2017-08-09', 'gold'])
    with pytest.raises(SystemExit):
        validate_args(p, args)
    _, err = capsys.readouterr()
    assert 'end date should be greater than start date' in err
