import datetime

import pytest

from db import Db, HistoricalPrices


@pytest.fixture
def db_path(tmpdir_factory):
    return str(tmpdir_factory.mktemp('data').join('test_db.sqlite'))


@pytest.fixture
def db(db_path):
    return Db(db_path)


@pytest.fixture
def gold_price(db):
    session = db.session
    price = HistoricalPrices(
        commodity='gold', date=datetime.date(2018, 2, 1), price='99.0')
    session.add(price)
    session.commit()
    return price
