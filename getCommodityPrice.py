#!/usr/bin/env python3

import argparse
import datetime
import os
import statistics

from db import DEFAULT_DB_PATH, Db


def get_date_from_arg(date_str):
    try:
        return datetime.datetime.strptime(date_str, '%Y-%m-%d').date()
    except ValueError:
        raise argparse.ArgumentTypeError('invalid date format')


def get_parser():
    parser = argparse.ArgumentParser(
        description="Prints  mean and variance of the commodity’s price over "
                    "the specified date range")
    parser.add_argument(
        'start_date', type=get_date_from_arg,
        help='Start date in format 2018-01-22')
    parser.add_argument(
        'end_date', type=get_date_from_arg,
        help='End date in format 2018-01-22')
    parser.add_argument(
        'commodity', choices=['silver', 'gold'], help='Commodity name')
    return parser


def validate_args(parser, args):
    if args.end_date < args.start_date:
        parser.error('end date should be greater than start date')


def print_commodity_stats(commodity, start_date, end_date):
    db = Db()
    prices = db.get_prices_between_dates(commodity, start_date, end_date)
    if prices:
        print('{} {:.4f} {:.4f}'.format(
            commodity, statistics.mean(prices), statistics.pvariance(prices)))
    else:
        print('No prices fetched for specified range')


if __name__ == '__main__':
    parser = get_parser()
    args = parser.parse_args()
    validate_args(parser, args)
    if not os.path.exists(DEFAULT_DB_PATH):
        parser.error('No db with scrapped data was found')
    print_commodity_stats(args.commodity, args.start_date, args.end_date)
